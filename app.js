// ユーザ入力
var target_name = "都０７:大島一丁目:門前仲町行:平日";

// curl -X POST -H "Content-Type: application/json" -d '{"value1":"15","value2":"東京駅前"}' https://maker.ifttt.com/trigger/bus_stop/with/key/dhKV7iz7nyfH2rDVZp76QP
var request = require('request');
function runPost(param1, param2, param3) {
  var options = {
    uri: "https://maker.ifttt.com/trigger/bus_stop/with/key/dhKV7iz7nyfH2rDVZp76QP",
    headers: {
      "Content-type": "application/json",
    },
    json: {
      "value1": param1,
      "value2": param2,
      "value3": param3
    }
  };

  request.post(options, function(error, response, body){});
}

function getDiff(date1Str, date2Str) {
  var date1 = new Date(date1Str);
  var date2 = new Date(date2Str);
 
  // getTimeメソッドで経過ミリ秒を取得し、２つの日付の差を求める
  var msDiff = date2.getTime() - date1.getTime();

  // 秒にする
  msDiff = msDiff / 1000;
  // 分にする
  msDiff = msDiff / 60;
  return msDiff;
}

var myStart = function(bus_stop) {
// 現在時刻
var now_time = new Date();
var hh = ('00' + now_time.getHours()).slice(-2);
var mm = ('00' + now_time.getMinutes()).slice(-2);
var my_now_time = `${hh}:${mm}`;
// console.log(my_now_time);

var json1 = require("./jsons/ToeibusHigashi20Timetable.json");
var json2 = require("./jsons/ToeibusHigashi22Timetable.json");
var json3 = require("./jsons/ToeibusTo07Timetable.json");

var jsons = [json1, json2, json3];
for (var i=0; i< jsons.length; i++) {
  var json = jsons[i];
  for (var j=0; j< json.length; j++) {
    var title = json[j]['dc:title'];
    // 自分のルート
    if (target_name === title) {
      var targets = json[j]['odpt:busstopPoleTimetableObject'];

      for (var k=0; k< targets.length; k++) {
        var target = targets[k];
        var time = target['odpt:departureTime'];
        var sign = target['odpt:destinationSign'];
        if (time >= my_now_time) {
          // 直近
          console.log(sign);
          console.log(time);

          // 現在時刻, 次
          var diff = getDiff(`2012/01/01 ${my_now_time}`, `2012/01/01 ${time}`);
          if (diff === 0) {
            diff = 'ぜろ';
          }
          console.log(diff);

          // 次 TODO: 終電は無視...
          target = targets[k+1];
          var next_time = target['odpt:departureTime'];
          console.log(next_time);

          var next_diff = getDiff(`2012/01/01 ${my_now_time}`, `2012/01/01 ${next_time}`);
          console.log(next_diff);
        //   console.log(bus_stop);

          runPost(diff, bus_stop, next_diff);
          break;
        }
      }
    }
  }
}

}


// var bus_stop = "門前仲町";

var sqlite = require('sqlite3').verbose();
// var db = new sqlite.Database('test.sqlite');
var db = new sqlite.Database('/Users/shinriyo/cek-handson-fortune/test.sqlite');

// 名前取得
var selectEach = function (condition, callback) {
  db.serialize(function () {
    db.each('select name from students', 
           function (err, res) {
             console.log('---');
             console.log(res.name);
             console.log('---');
            //  bus_stop = res;
             myStart(res.name);

             if (err) throw err;
             callback(err);
    });
  });
};

var hoge = { id: 0 };
selectEach(hoge, (a) => { });
// console.log(bus_stop);