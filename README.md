LIFF
LINE Front-end Framework (LIFF) 
https://developers.line.me/en/docs/liff/overview/

ビジネスワーク部



https://developer.cybozu.io/hc/ja/articles/200929540?utm_source=web&utm_medium=devnet&utm_campaign=topbanner

## 起動

IFTTT

```
node app.js
```

## cron設定

```
crontab cron.conf
```

## 削除

```
crontab -r
```

# 登録サーバ

```
cd cek-handson-fortune
ngrok http 3000
```
